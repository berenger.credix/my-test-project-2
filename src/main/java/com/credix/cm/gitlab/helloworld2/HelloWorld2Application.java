package com.credix.cm.gitlab.helloworld2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloWorld2Application {

    public static void main(String[] args) {
        System.out.println("Hello World Berenger!");
    }

}
